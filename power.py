class Power:
	def __init__(self, init_row_pos, init_column_pos, init_row_speed, init_column_speed, ptype):
		self.row_pos = init_row_pos
		self.column_pos = init_column_pos
		self.row_speed = init_row_speed
		self.column_speed = init_column_speed
		self.type = ptype
		self.frames = 0
		self.last_gravity_frame = 0

	def update_position(self, grid_row_length, grid_column_length):
		self.last_gravity_frame += 1
		if(self.last_gravity_frame > 3 and self.row_speed < 2):
			self.row_speed += 1
		new_row_pos = self.row_pos + self.row_speed
		new_column_pos = self.column_pos + self.column_speed
		if(new_row_pos < 2):
			new_row_pos = 2 - new_row_pos
			self.row_speed *= -1
		if(new_column_pos < 2):
			new_column_pos = 2 - new_column_pos
			self.column_speed *= -1
		if(new_column_pos > grid_column_length - 3):
			new_column_pos = 2 * grid_column_length - 4 - new_column_pos
			self.column_speed *= -1
		paddle_cross = False
		if(new_row_pos >= grid_row_length - 3 and self.row_pos < grid_row_length - 3):
			paddle_cross = True
		self.row_pos = new_row_pos
		self.column_pos = new_column_pos
		return paddle_cross

	def get_row_pos(self):
		return self.row_pos

	def get_column_pos(self):
		return self.column_pos

	def get_type(self):
		return self.type

	def add_frame(self):
		self.frames += 1

	def get_frames(self):
		return self.frames