import sys
import os
import time
import termios
import atexit
import math
import copy
import random
from colorama import Fore, Back, Style

from paddle import Paddle
from grid import Grid
from ball import Ball, BBall
from brick import Brick
from power import Power
from boss import Boss
from bomb import Bomb
from bullet import Bullet

old_settings = None

def init_anykey():
    global old_settings
    old_settings = termios.tcgetattr(sys.stdin)
    new_settings = termios.tcgetattr(sys.stdin)
    new_settings[3] = new_settings[3] & ~(termios.ECHO | termios.ICANON) # lflags
    new_settings[6][termios.VMIN] = 0  # cc
    new_settings[6][termios.VTIME] = 0 # cc
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, new_settings)

@atexit.register
def term_anykey():
    global old_settings
    os.system('clear')
    print("Game has ended")
    if(boss_health <= 0):
    	print("Boss has been defeated!\nCongratulations")
    else:
    	print("Better luck next time!")
    print("Score : ", score ,"\tTime Played : ", int(time.time() - init_time))
    if old_settings:
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)

def anykey():
    ch_set = []
    ch = os.read(sys.stdin.fileno(), 1)
    while ch != None and len(ch) > 0:
        ch_set.append(chr(ch[0]))
        ch = os.read(sys.stdin.fileno(), 1)
    if(len(ch_set) == 0):
    	return "None"
    else:
    	return (ch_set[0])

# game variables
grid = ""
grid_row_length = 32
grid_column_length = 62
paddle = ""
paddle_speed = 2
paddle_length = 10
unit_time = 0.1
balls = []
ball_row_speed = -2
bricks = []
powers = []
paddle_powers = []
power_life = 35
boss = ""
score = 0
lives_left = 5
current_level = 1
init_time = time.time()
paddle_hold = True
power_hold = False
ball_thru = False
boss_level = 3
boss_health = 100
bombs = []
last_bomb_time = time.time()
last_level_increment_time = time.time()
last_bullet_time = time.time()
bricks_reach = False
bricks_move_time = 10
bullets = []
paddle_shoot = False
boss_layer_1 = False
boss_layer_2 = False

def init_screen():
    global grid, paddle, balls, boss
    random.seed()
    paddle = Paddle(paddle_length, paddle_speed, grid_column_length)
    grid = Grid(grid_row_length, grid_column_length)
    boss = Boss(grid_column_length)
    new_screen(True)
    

def new_screen(level_increment):
	global balls, paddle_powers, power_hold, ball_thru, bricks, paddle_shoot, powers
	balls.clear()
	balls.append(Ball(ball_row_speed, grid_row_length, grid_column_length))
	for power in paddle_powers:
		ptype = power.get_type()
		paddle_powers.remove(power)
		if(ptype == "E"):
			paddle.decrease_length()
		elif(ptype == "S"):
			paddle.increase_length()
		elif(ptype == "G"):
			power_hold = False
		elif(ptype == "F"):
			for ball in balls:
				ball.off_fast()
		elif(ptype == "T"):
			ball_thru = False
			for ball in balls:
				ball.set_thru(False)
		elif(ptype == "Y"):
			paddle_shoot = False
	if(level_increment):
	    powers.clear()
	    bricks.clear()
	    file_handle = open("level_{}.txt".format(current_level), "r")
	    lines_list = file_handle.readlines()
	    file_handle.close()
	    for line in lines_list:
	        snum = line.split(", ")
	        is_rainbow = True
	        if(random.random() < 0.6 or snum[2] == -1):
	        	is_rainbow = False
	        bricks.append(Brick(int(snum[0]), int(snum[1]), int(snum[2]), is_rainbow))
	else:
		move_bricks_down()

def move_bricks_down():
	global bricks, bricks_reach
	if(current_level == boss_level):
		return
	if(time.time() - last_level_increment_time > bricks_move_time):
		for brick in bricks:
			brick.move_down()
			if(brick.get_row_pos() + 1 >= grid_row_length - 3):
				bricks_reach = True

def do_brick_collisions(collide_row, collide_column, power_row_speed, power_column_speed):
	brick_collide = False
	global bricks, score, powers
	for brick in bricks:
		if(brick.has_hit(collide_row, collide_column)):
		    brick_collide = True
		    brick.off_rainbow()
		    cur_strength = brick.get_strength()
		    if(cur_strength == 3):
		        score += 10
		    elif(cur_strength == 2):
		        score += 15
		    elif(cur_strength == 1):
		        score += 20
		        choice = int(random.random() * 9)
		        ptype = " "
		        if(choice == 1):
		        	ptype = "E"
		        elif(choice == 2):
		        	ptype = "S"
		        elif(choice == 3):
		        	ptype = "G"
		        elif(choice == 4):
		        	ptype = "F"
		        elif(choice == 5):
		        	ptype = "B"
		        elif(choice == 6):
		        	ptype = "T"
		        elif(choice == 7):
		        	ptype = "Y"
		        if(ptype != " " and current_level != boss_level):
		            powers.append(Power(brick.get_row_pos() + 1, brick.get_column_pos() + 2, power_row_speed, power_column_speed, ptype))
		    brick.decrease_strength(ball_thru)
		    break
	return brick_collide


def print_screen():
    print("Score : ", score, "\tLives Left : ", lives_left, "\tLevel :", current_level, "\tTime Played : ", int(time.time() - init_time), end = "")
    if(current_level == boss_level):
    	health_bar = ""
    	for i in range(int(boss_health / 10)):
    		health_bar += "#"
    	print("\t\t    Boss Health : ", health_bar, end = "")
    print()
    tempgrid = grid.get_grid()
    for row in range(grid_row_length):
        for column in range(grid_column_length):
        	ch = tempgrid[row][column]
        	if(ch == "^"):
        		ch = Fore.GREEN + "^" + Style.RESET_ALL
        	if(ch == "@"):
        		ch = Fore.RED + "@" + Style.RESET_ALL
        	print(ch, end = " ")
        print()
    for power in paddle_powers:
    	print(power.get_type() + " : " + str(int((power_life - power.get_frames()) * 100 / power_life)) + "%", end = " ")
    print()

def update_screen():
    global grid, balls, score, paddle_hold, powers, paddle_powers, power_hold, ball_thru, boss, boss_health, lives_left, last_bomb_time, paddle_shoot, last_bullet_time, boss_layer_1, boss_layer_2
    tempgrid = copy.deepcopy(grid.get_grid())
    grid.reset_grid()
    newgrid = copy.deepcopy(grid.get_grid())

    # for paddle
    for i in range(paddle.get_length()):
        newgrid[grid_row_length - 3][i + paddle.get_position()] = Fore.GREEN + "#" + Style.RESET_ALL
    if(paddle_shoot):
    	newgrid[grid_row_length - 4][paddle.get_position()] = Fore.RED + "=" + Style.RESET_ALL
    	newgrid[grid_row_length - 4][paddle.get_position() + paddle.get_length() - 1] = Fore.RED + "=" + Style.RESET_ALL

    # for balls
    for ball in balls:
        ball_exist = True
        if(ball.get_hold()):
        	paddle_hold = True
        	ball.do_hold(paddle.get_position() + int(paddle.get_length() / 2))
        else:
        	temp_ball_row_speed = ball.get_row_speed()
        	temp_ball_column_speed = ball.get_column_speed()
	        collisions_list = ball.update_position(tempgrid)
	        for collisions in collisions_list:
	            if(collisions[0] == 'w' and collisions[1] == grid_row_length - 1):
	                balls.remove(ball)
	                ball_exist = False
	            elif(collisions[0] == 'p'):
	                factor = int(math.floor(((collisions[2] - paddle.get_position()) / paddle.get_length()) * 5))
	                ball.change_speed(factor - 2)
	                if(power_hold):
	                	ball.set_hold(True)
	                move_bricks_down()
	            elif(collisions[0] == 'b'):
	                brick_collide = do_brick_collisions(collisions[1], collisions[2], temp_ball_row_speed, temp_ball_column_speed)
	                if(not(brick_collide) and current_level == boss_level):
	                	boss_health -= 20
	                	score += 100
        if(ball_exist):
            newgrid[ball.get_row_pos()][ball.get_column_pos()] = Fore.WHITE + "o" + Style.RESET_ALL

    # for bricks
    for brick in bricks:
    	brick.update_rainbow(int(random.random() * 3) + 1)
    	cur_x = brick.get_row_pos()
    	cur_y = brick.get_column_pos()
    	cur_strength = brick.get_strength()
    	asc = ""
    	dsc = Style.RESET_ALL
    	if(cur_strength == 3):
    		asc = Fore.GREEN
    	elif(cur_strength == 2):
    		asc = Fore.YELLOW
    	elif(cur_strength == 1):
    		asc = Fore.RED
    	elif(cur_strength == 0):
    		continue
    	elif(cur_strength == -1):
    		asc = Fore.BLUE
    	newgrid[cur_x][cur_y] = asc + "|" + dsc
    	newgrid[cur_x][cur_y + 1] = asc + "*" + dsc
    	newgrid[cur_x][cur_y + 2] = asc + "*" + dsc
    	newgrid[cur_x][cur_y + 3] = asc + "*" + dsc
    	newgrid[cur_x][cur_y + 4] = asc + "|" + dsc
    	newgrid[cur_x + 1][cur_y] = asc + "|" + dsc
    	newgrid[cur_x + 1][cur_y + 1] = asc + "_" + dsc
    	newgrid[cur_x + 1][cur_y + 2] = asc + "_" + dsc
    	newgrid[cur_x + 1][cur_y + 3] = asc + "_" + dsc
    	newgrid[cur_x + 1][cur_y + 4] = asc + "|" + dsc

    # for powers
    for power in powers:
    	paddle_cross = power.update_position(grid_row_length, grid_column_length)
    	powrow = power.get_row_pos()
    	powcol = power.get_column_pos()
    	if(paddle_cross and powcol >= paddle.get_position() and powcol <= paddle.get_position() + paddle.get_length() - 1):
    		paddle_powers.append(power)
    		powers.remove(power)
    	elif(powrow >= grid_row_length - 1):
    		powers.remove(power)
    	else:
    		newgrid[powrow][powcol] = power.get_type()

    # for paddle powers
    for power in paddle_powers:
    	power.add_frame()
    	powframe = power.get_frames()
    	ptype = power.get_type()
    	if(powframe == 1):
    		if(ptype == "E"):
    			paddle.increase_length()
    		elif(ptype == "S"):
    			paddle.decrease_length()
    		elif(ptype == "G"):
    			power_hold = True
    		elif(ptype == "F"):
    			for ball in balls:
    				ball.on_fast()
    		elif(ptype == "B"):
    			if(len(balls) >= 4):
    				continue
    			bballs = copy.deepcopy(balls)
    			for ball in balls:
    				if(not(ball.get_hold())):
    					bballs.append(BBall(ball.get_row_pos(), ball.get_column_pos(), ball.get_row_speed(), -ball.get_column_speed(), grid_row_length, grid_column_length))
    			balls.clear()
    			balls = copy.deepcopy(bballs)
    			bballs.clear()
    		elif(ptype == "T"):
    			ball_thru = True
    			for ball in balls:
    				ball.set_thru(True)
    		elif(ptype == "Y"):
    			paddle_shoot = True
    	elif(powframe > power_life):
    		paddle_powers.remove(power)
    		if(ptype == "E"):
    			paddle.decrease_length()
    		elif(ptype == "S"):
    			paddle.increase_length()
    		elif(ptype == "G"):
    			power_hold = False
    		elif(ptype == "F"):
    			for ball in balls:
    				ball.off_fast()
    		elif(ptype == "B"):
    			cur_len = len(balls)
    			for i in range(int(cur_len / 2)):
    				balls.remove(balls[0])
    		elif(ptype == "T"):
    			ball_thru = False
    			for ball in balls:
    				ball.set_thru(False)
    		elif(ptype == "Y"):
    			paddle_shoot = False

    # for boss
    if(current_level == boss_level):
    	boss.update_position(paddle.get_position() + int(paddle.get_length() / 2), 2)
    	boss_pos = boss.get_position()
    	boss_rows = boss.get_row_length()
    	boss_columns = boss.get_column_length()
    	for row in range(boss_rows):
    		for column in range(boss_columns):
    			newgrid[1 + row][boss_pos + column] = Fore.MAGENTA + boss.get_block_value(row, column) + Style.RESET_ALL
    	if(boss_health <= 60 and not(boss_layer_1) and balls[0].get_row_pos() > 15):
    		boss_layer_1 = True
    		for spawn_column in range(0, 12):
    			bricks.append(Brick(13, 1 + spawn_column * 5, 1, False))
    	elif(boss_health <= 20 and not(boss_layer_2) and balls[0].get_row_pos() > 17):
    		boss_layer_2 = True
    		for spawn_column in range(0, 12):
    			bricks.append(Brick(15, 1 + spawn_column * 5, 1, False))

    #drop bombs
    if(current_level == boss_level):
    	if(time.time() - last_bomb_time > 2):
    		last_bomb_time = time.time()
    		bombs.append(Bomb(2 + boss.get_row_length() - 1, boss.get_position() + int(boss.get_column_length() / 2)))
    	for bomb in bombs:
    		bomb.move_down()
    		if(bomb.get_row_pos() == grid_row_length - 1):
    			bombs.remove(bomb)
    		elif(bomb.get_row_pos() == grid_row_length - 3 and bomb.get_column_pos() >= paddle.get_position() and bomb.get_column_pos() <= paddle.get_position() + paddle.get_length() - 1):
    			bombs.remove(bomb)
    			lives_left -= 1
    		else:
    			newgrid[bomb.get_row_pos()][bomb.get_column_pos()] = "@"

    #for bullets
    if(paddle_shoot):
    	if(time.time() - last_bullet_time > 0.2):
    		last_bullet_time = time.time()
    		bullets.append(Bullet(grid_row_length - 4, paddle.get_position()))
    		bullets.append(Bullet(grid_row_length - 4, paddle.get_position() + paddle.get_length() - 1))
    for bullet in bullets:
    	bullet.move_up()
    	if(bullet.get_row_pos() == 0):
    		bullets.remove(bullet)
    	else:
    		brick_collide = do_brick_collisions(bullet.get_row_pos(), bullet.get_column_pos(), 1, 0)
    		if(brick_collide):
    			bullets.remove(bullet)
    		else:
    			newgrid[bullet.get_row_pos()][bullet.get_column_pos()] = "^"

    grid.set_grid(newgrid)

init_anykey()
init_screen()
flag = "None"
flag_ctr = 0

while(True):
    os.system('clear')
    print_screen()
    # stopping program
    if(bricks_reach):
    	break
    if(lives_left == 0):
    	break
    if(current_level == boss_level):
    	if(boss_health <= 0):
    		break
    else:
        bricks_left = 0
        for brick in bricks:
        	if(brick.get_strength() > 0):
        		bricks_left += 1
        if(bricks_left == 0):
        	current_level += 1
        	new_screen(True)
    if(len(balls) == 0):
    	lives_left -= 1
    	if(lives_left > 0):
    		new_screen(False)
    update_screen()
    key = anykey()
    # optimisation for key hold "None" issue
    if(key != "None"):
        flag = key
        flag_ctr = 2
    if(key == "None" and flag_ctr > 0):
        key = flag
        flag_ctr -= 1
    # moving paddle
    if(key == 'a'):
    	paddle.move_left()
    elif(key == 'd'):
    	paddle.move_right()
    elif(key == '+'):
        paddle.increase_length()
    elif(key == '-'):
        paddle.decrease_length()
    elif(key == ' '):
    	if(paddle_hold):
            paddle_hold = False
            for ball in balls:
            	if(ball.get_hold()):
                	ball.set_hold(False)
    elif(key == 'q'):
    	break
    elif(key == 'n' and time.time() - last_level_increment_time > 3):
    	last_level_increment_time = time.time()
    	if(current_level == boss_level):
    		break
    	else:
    		current_level += 1
    		new_screen(True)
    elif(key == 'b'):
    	if(time.time() - last_bullet_time > 0.2):
    		last_bullet_time = time.time()
    		bullets.append(Bullet(grid_row_length - 3, paddle.get_position()))
    		bullets.append(Bullet(grid_row_length - 3, paddle.get_position() + paddle.get_length() - 1))
    time.sleep(unit_time)