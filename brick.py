class Brick:
	def __init__(self, init_pos_row, init_pos_column, init_strength, init_rainbow):
		self.row_pos = init_pos_row
		self.column_pos = init_pos_column
		self.strength = init_strength
		self.is_rainbow = init_rainbow

	def get_row_pos(self):
		return self.row_pos

	def get_column_pos(self):
		return self.column_pos

	def get_strength(self):
		return self.strength

	def has_hit(self, hit_row, hit_column):
		if(not(self.strength == 0) and hit_row >= self.row_pos and hit_row <= self.row_pos + 1 and hit_column >= self.column_pos and hit_column <= self.column_pos + 4):
			return True
		else:
			return False

	def decrease_strength(self, ball_thru):
		if(ball_thru):
			self.strength = 0
		if(self.strength > 0):
			self.strength -= 1

	def move_down(self):
		self.row_pos += 1

	def off_rainbow(self):
		self.is_rainbow = False

	def update_rainbow(self, new_strength):
		if(self.strength > 0 and self.is_rainbow):
			self.strength = new_strength