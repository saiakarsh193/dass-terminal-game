class Paddle:
	def __init__(self, paddle_length, paddle_speed, grid_column_length):
		self.length = paddle_length
		self.speed = paddle_speed
		self.column_length = grid_column_length
		self.position = int((self.column_length - self.length) / 2)

	def increase_length(self):
		if(self.length + 2 > int(self.column_length / 2)):
			return
		if(self.position + self.length - 1 == self.column_length - 2):
			self.position -= 2
		elif(self.position > 1):
			self.position -= 1
		self.length += 2

	def decrease_length(self):
		if(self.length - 2 <= 1):
			return
		self.length -= 2
		self.position += 1

	def get_length(self):
		return self.length

	def get_position(self):
		return self.position

	def move_left(self):
		if(self.position - self.speed >= 1):
			self.position -= self.speed
		else:
			self.position = 1

	def move_right(self):
		lastpos = self.position + self.length - 1
		if(lastpos + self.speed <= self.column_length - 2):
			self.position += self.speed
		else:
			self.position = self.column_length - self.length - 1