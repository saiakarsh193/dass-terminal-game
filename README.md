Game Description:
This game is a terminal game based on DX-Ball. In this game there is a paddle which is used to bounce balls. The balls collide with various types of bricks which in turn give power ups and score. By collecting the power ups using the paddle we get different abilities. We loose a ball if it reaches the floor. If we lose all balls then we lose a life, and if we lose all lives then the game ends. Our objective is to get as high score as possible.

This is a continuation of the previous assignment. Here i will discuss about the new features and rules. There are three levels in the game. You can go to the next level by destroying all the destructible bricks. The lives and score will be carried forward in every level. The bricks in each level (except boss level) will start coming down after some duration every time the ball hits the paddle. If any brick reaches the paddle level, the game is lost. A new type of bricks has been added which keeps on changing its strength untill it gets hit by the ball. The powerups follow the trajectory of the ball on spawn and also have a gravity effect. A new power up, shooting paddle has been added. The last level is the boss which is an UFO capable of dropping of bombs which on contact takes one life. The UFO can also spawn defensive bricks to save itself. You can see the boss health on the top right bar.
Keys:
Press 'a' to move the paddle left
Press 'd' to move the paddle right
Press ' ' to release the ball (During start of the round or when hold power up has been collected)
Press 'n' to skip level during testing

Start game:
`python3 main.py`

Power Ups:
Power Up 'E' - Expands the paddle
Power Up 'S' - Shrinks the paddle
Power Up 'G' - Paddle will Grab the ball on contact and can relauch it using spacebar
Power Up 'F' - Makes the balls go Faster
Power Up 'T' - Gives the balls ability to go Thru bricks (even the Unbreakable bricks)
Power Up 'B' - Duplicates the Balls
Power Up 'Y' - Activates the dual cannons for the paddle
