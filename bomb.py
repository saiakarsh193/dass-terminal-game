class Bomb:
	def __init__(self, row_pos, column_pos):
		self.row_pos = row_pos
		self.column_pos = column_pos

	def get_row_pos(self):
		return self.row_pos

	def get_column_pos(self):
		return self.column_pos

	def move_down(self):
		self.row_pos += 1