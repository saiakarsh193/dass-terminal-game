class Boss:
	def __init__(self, grid_column_length):
		self.position = 4
		self.speed = 1
		self.row_length = 6
		self.column_length = 21
		self.grid_column_length = grid_column_length
		self.block = []
		self.block.append(r"       _______       ")
		self.block.append(r"      /       \      ")
		self.block.append(r"     /         \     ")
		self.block.append(r" _.-~===========~-._ ")
		self.block.append(r"(___________________)")
		self.block.append(r"      \_______/      ")

	def get_block_value(self, row, column):
		return self.block[row][column]

	def get_position(self):
		return self.position

	def update_position(self, target, tolerance):
		difference = (self.position + self.column_length / 2) - target
		if(abs(difference) > tolerance):
			if(difference > 0):
				self.move_left()
			else:
				self.move_right()

	def get_row_length(self):
		return self.row_length

	def get_column_length(self):
		return self.column_length

	def move_left(self):
		if(self.position - self.speed >= 1):
			self.position -= self.speed
		else:
			self.position = 1

	def move_right(self):
		lastpos = self.position + self.column_length - 1
		if(lastpos + self.speed <= self.grid_column_length - 2):
			self.position += self.speed
		else:
			self.position = self.grid_column_length - self.column_length - 1