class Grid:
    def __init__(self, grid_row_length, grid_column_length):
	    self.grid = [[" "] * grid_column_length for _ in range(grid_row_length)]
	    self.row_length = grid_row_length
	    self.column_length = grid_column_length
	    for row in range(self.row_length):
	        for column in range(self.column_length):
	            if(row == 0):
	                self.grid[row][column] = "_"
	            elif(row == self.row_length - 1):
	            	self.grid[row][column] = "\""
	            elif(column == 0 or column == self.column_length - 1):
	                self.grid[row][column] = "|"
	            else:
	                self.grid[row][column] = " "

    def get_grid(self):
        return self.grid

    def reset_grid(self):
        for row in range(1, self.row_length - 1):
            for column in range(1, self.column_length - 1):
                self.grid[row][column] = " "

    def set_grid(self, new_grid):
        self.grid = new_grid