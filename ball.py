class Ball:
	def __init__(self, ball_row_speed, grid_row_length, grid_column_length):
		self.row_pos = 0
		self.column_pos = 0
		self.const_row_speed = ball_row_speed
		self.row_speed = ball_row_speed
		self.column_speed = 1
		self.row_length = grid_row_length
		self.column_length = grid_column_length
		self.on_hold = True
		self.on_thru = False
		self.count_hold = 0

	def __do_collisions(self, tempgrid, cr, cc, collision_list):
		# return 0 means vertical collision
		# return 1 means horizontal collision
		if(cr == 0 or cr == self.row_length - 1 or cc == 0 or cc == self.column_length - 1): # wall collision
			collision_list.append(('w', cr, cc))
			if(cr == 0 or cr == self.row_length - 1):
				self.row_speed *= -1
				return 0
			else:
				self.column_speed *= -1
				return 1
		elif(cr == self.row_length - 3): # paddle collision
			collision_list.append(('p', cr, cc))
			self.row_speed *= -1
			return 0
		else: # brick collision
			collision_list.append(('b', cr, cc))
			if(self.on_thru):
				return -1
			if(tempgrid[cr][cc] == "|"):
				self.column_speed *= -1
				return 1
			else:
				self.row_speed *= -1
				return 0

	def update_position(self, tempgrid):
		collision_list = []
		ignore_chars = [" ", "E", "S", "F", "B", "G", "T", "Y", "@", "^"]
		row_previous = self.row_pos
		column_previous = self.column_pos
		if(abs(self.column_speed) >= abs(self.row_speed)):
			cols = [abs(int(self.column_speed / self.row_speed))] * abs(self.row_speed)
			cols_left = abs(self.column_speed) - cols[0] * abs(self.row_speed)
			for i in range(cols_left):
				cols[i] += 1
			for row in range(abs(self.row_speed)):
				update_row = True
				for column in range(cols[row]):
					cr = int(self.row_speed / abs(self.row_speed)) + row_previous
					cc = int(self.column_speed / abs(self.column_speed)) + column_previous
					update_column = True
					if(not(tempgrid[cr][cc] in ignore_chars)):
						type_collision = self.__do_collisions(tempgrid, cr, cc,collision_list)
						if(type_collision == 1):
							update_column = False # horizontal collision
						elif(type_collision == 0):
							update_row = False # vertical collision
					if(update_column and tempgrid[row_previous][cc] == " "):
						column_previous = cc
				if(update_row and tempgrid[cr][column_previous] == " "):
					row_previous = cr
		else:
			column_sub = self.column_speed # column speed substitute
			if(self.column_speed == 0):
				column_sub = 1
			rows = [abs(int(self.row_speed / column_sub))] * abs(column_sub)
			rows_left = abs(self.row_speed) - rows[0] * abs(column_sub)
			for i in range(rows_left):
				rows[i] += 1
			for column in range(abs(column_sub)):
				update_column = True
				for row in range(rows[column]):
					cr = int(self.row_speed / abs(self.row_speed)) + row_previous
					if(self.column_speed == 0):
						cc = column_previous
					else:
						cc = int(self.column_speed / abs(self.column_speed)) + column_previous
					update_row = True
					if(not(tempgrid[cr][cc] in ignore_chars)):
						type_collision = self.__do_collisions(tempgrid, cr, cc,collision_list)
						if(type_collision == 1):
							update_column = False # horizontal collision
						elif(type_collision == 0):
							update_row = False # vertical collision
					if(update_row and tempgrid[cr][column_previous] == " "):
						row_previous = cr
				if(update_column and tempgrid[row_previous][cc] == " "):
					column_previous = cc
		if(self.row_pos == row_previous and self.column_pos == column_previous):
			self.count_hold += 1
			if(self.count_hold == 3):
				self.count_hold = 0
				self.row_speed *= -1
				self.column_speed *= -1
		else:
			self.count_hold = 0
		self.row_pos = row_previous
		self.column_pos = column_previous
		return collision_list

	def get_row_pos(self):
		return self.row_pos

	def get_column_pos(self):
		return self.column_pos

	def get_row_speed(self):
		return self.row_speed

	def get_column_speed(self):
		return self.column_speed

	def get_hold(self):
		return self.on_hold

	def set_hold(self, value):
		self.on_hold = value

	def do_hold(self, paddle_mid):
		self.row_pos = self.row_length - 4
		self.column_pos = paddle_mid

	def change_speed(self, factor):
		new_speed = self.column_speed + factor
		if(new_speed < -2):
			new_speed = -2
		elif(new_speed > 2):
			new_speed = 2
		self.column_speed = new_speed

	def on_fast(self):
		if(self.column_speed == 0):
			column_sign = 0
		else:
			column_sign = int(self.column_speed / abs(self.column_speed))
		self.column_speed = 2 * column_sign
		self.row_speed = int(self.row_speed / abs(self.row_speed)) * (abs(self.const_row_speed) + 1)

	def off_fast(self):
		if(self.column_speed == 0):
			column_sign = 0
		else:
			column_sign = int(self.column_speed / abs(self.column_speed))
		self.column_speed = 1 * column_sign
		self.row_speed = int(self.row_speed / abs(self.row_speed)) * abs(self.const_row_speed)

	def set_thru(self, value):
		self.on_thru = value

class BBall(Ball):
	def __init__(self, init_row_pos, init_column_pos, ball_row_speed, ball_column_speed, grid_row_length, grid_column_length):
		self.row_pos = init_row_pos
		self.column_pos = init_column_pos
		self.row_speed = ball_row_speed
		if(ball_column_speed == 0):
			ball_column_speed = 1
		self.column_speed = ball_column_speed
		self.row_length = grid_row_length
		self.column_length = grid_column_length
		self.on_hold = False
		self.on_thru = False